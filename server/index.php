<?php

// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// [ 应用入口文件 ]
namespace think;




ini_set("display_errors", "stderr");  //ini_set函数作用：为一个配置选项设置值，
error_reporting(E_ALL);     //显示所有的错误信息
// 加载基础文件


// 定义应用目录
define('APP_PATH', dirname(__DIR__) . '/core/application');
// 加载框架基础引导文件
require dirname(__DIR__).'/core/vendor/autoload.php';
require dirname(__DIR__).'/core/thinkphp/base.php';


// 支持事先使用静态方法设置Request对象和Config对象
// 定义SESSION保存目录
//define('SESSION_PATH','./session/');
// 执行应用并响应
Container::get('app')->path(APP_PATH)->run()->send();

