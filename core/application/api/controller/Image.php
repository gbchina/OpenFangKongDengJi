<?php


namespace app\api\controller;


use PHPQrcode\Qrcode;
use think\facade\Response;

class Image
{
    function QrcodeGenerator($content, $label)
    {
// $content 一般为url地址 当然也可以是文字内容

        $qrCode = new \Endroid\QrCode\QrCode();
        $qrCode->setText($content);
        $qrCode->setSize(300);
        $qrCode->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0));
        $qrCode->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0));
        $qrCode->setLabel($label);
        $qrCode->setLabelFontSize(14);
// 指定内容类型
        return Response::data($qrCode->writeString())->header(
            ['Content-Type: ' => $qrCode->getContentType()]
        );

    }
}