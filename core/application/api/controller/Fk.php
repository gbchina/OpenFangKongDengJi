<?php


namespace app\api\controller;


use app\api\model\FkInfo;


use Endroid\QrCode\QrCode;
use think\Controller;
use think\facade\Request;


class Fk extends Controller
{
    function fk_init()
    {
        if (Request::isPost()) {
            $request = Request::put();
            $fkModel = new FkInfo();
            if ($reponse = $fkModel->where($request)->order('id desc')->find()){
                return $reponse;
            }
        }
    }

    function receiver()
    {
        if (Request::isPost()) {
            $request = Request::put();
            if ($request) {
                $fkModel = new FkInfo();
                if ($response = $fkModel->where($request)->find()) {
                    $response['ipaddr'] = Request::ip();
                    $response['updatetime'] = time();
                    $response->save();
                    $uuid = $response['uuid'];
                    $housing = $response['housing_name'];
                } else {
                    unset($request['id']);
                    $request['ipaddr'] = Request::ip();
                    $request['updatetime'] = time();
                    $uuid = $request['uuid'] = uuid();
                    $housing = $request['housing_name'];
                    $fkModel->save($request);

                }
                if ($uuid) {
                    $qr_img['content'] = 'http://' . Request::server('HTTP_HOST') . '/page/registration.html?fk_uuid=' . $uuid;
                    $qr_img['label'] = $housing . '小区疫情防控登记';
                    return $qr_img;
                }
            }
        }
    }
}