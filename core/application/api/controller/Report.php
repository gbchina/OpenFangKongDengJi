<?php


namespace app\api\controller;


use app\api\model\DjInfo;
use app\api\model\FkInfo;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use think\Response;

class Report
{
    function index()
    {
    }

    function export($uuid)
    {
        $fkModel = new FkInfo();
        $reports = $fkModel->with('record')->where('uuid', $uuid)->all();
        $sheet = new Spreadsheet();  //创建一个新的excel文档
        $filename = date('Y-m-d-H-i-s', time()) . $reports[0]['company_name'] . '包联单位疫情防控报表.xlsx';
        foreach ($reports as $key => $report) {

            $company_name = $report['company_name'];

            $objSheet = $sheet->getActiveSheet();  //获取当前操作sheet的对象
            $activeSheetTitle = date('Y-m-d', time()) . $company_name . '包联单位疫情防控报表' . $key;
            $objSheet->setTitle($activeSheetTitle);  //设置当前sheet的标题
            //设置宽度为true,不然太窄了
            $objSheet->getColumnDimension('A')->setAutoSize(true);
            $objSheet->getColumnDimension('B')->setAutoSize(true);
            $objSheet->getColumnDimension('C')->setAutoSize(true);
            $objSheet->getColumnDimension('D')->setAutoSize(true);
            $objSheet->getColumnDimension('E')->setAutoSize(true);
            $objSheet->getColumnDimension('F')->setAutoSize(true);
            $objSheet->getColumnDimension('G')->setAutoSize(true);
            $objSheet->getColumnDimension('H')->setAutoSize(true);
            $objSheet->getColumnDimension('I')->setAutoSize(true);
            $objSheet->getColumnDimension('J')->setAutoSize(true);
            $objSheet->getColumnDimension('K')->setAutoSize(true);
            $objSheet->getColumnDimension('L')->setAutoSize(true);
            $objSheet->getColumnDimension('M')->setAutoSize(true);
            $objSheet->getColumnDimension('N')->setAutoSize(true);
            $objSheet->getColumnDimension('O')->setAutoSize(true);
            $objSheet->getColumnDimension('P')->setAutoSize(true);
            $objSheet->getColumnDimension('Q')->setAutoSize(true);
            $objSheet->getColumnDimension('R')->setAutoSize(true);
            $objSheet->getColumnDimension('S')->setAutoSize(true);
            $objSheet->getColumnDimension('T')->setAutoSize(true);
            $objSheet->getColumnDimension('U')->setAutoSize(true);
            $objSheet->getColumnDimension('V')->setAutoSize(true);
            $objSheet->getColumnDimension('W')->setAutoSize(true);
            //设置第一栏的标题
            $objSheet
                ->setCellValue('A1', '防控单位')
                ->setCellValue('B1', '防控区域')
                ->setCellValue('C1', '出入人员')
                ->setCellValue('D1', '性别')
                ->setCellValue('E1', '电话')
                ->setCellValue('F1', '地址')
                ->setCellValue('G1', '所在区域')
                ->setCellValue('H1', '身份证')
                ->setCellValue('I1', '住户类型')
                ->setCellValue('J1', '工作单位')
                ->setCellValue('K1', '体温')
                ->setCellValue('L1', '家庭成员是否正常')
                ->setCellValue('M1', '家庭成员数')
                ->setCellValue('N1', '车牌号')
                ->setCellValue('O1', '是否驾车')
                ->setCellValue('P1', '进出方向')
                ->setCellValue('Q1', '省外归来')
                ->setCellValue('R1', '盟外归来')
                ->setCellValue('S1', '归来区域')
                ->setCellValue('T1', '归来时间')
                ->setCellValue('U1', '出入备注')
                ->setCellValue('V1', '强制出入备注')
                ->setCellValue('W1', '本条记录时间');
            $startLine = 2;
            foreach ($report->record as $line) {
                $objSheet
                    ->setCellValue('A' . $startLine, $report->company_name)
                    ->setCellValue('B' . $startLine, $report->housing_name)
                    ->setCellValue('C' . $startLine, $line->name)
                    ->setCellValue('D' . $startLine, $line->sex)
                    ->setCellValue('E' . $startLine, '`'.$line->phone)
                    ->setCellValue('F' . $startLine, $line->address)
                    ->setCellValue('G' . $startLine, $line->housing_name)
                    ->setCellValue('H' . $startLine, '`'.$line->idcard)
                    ->setCellValue('I' . $startLine, $line->type)
                    ->setCellValue('J' . $startLine, $line->company_name)
                    ->setCellValue('K' . $startLine, $line->temperature)
                    ->setCellValue('L' . $startLine, $line->is_family_temperature_normal)
                    ->setCellValue('M' . $startLine, $line->family_count)
                    ->setCellValue('N' . $startLine, $line->car_number)
                    ->setCellValue('O' . $startLine, $line->have_car)
                    ->setCellValue('P' . $startLine, $line->direction)
                    ->setCellValue('Q' . $startLine, $line->is_autonomous_region_migrants)
                    ->setCellValue('R' . $startLine, $line->is_city_region_migrants)
                    ->setCellValue('S' . $startLine, $line->from_area_name)
                    ->setCellValue('T' . $startLine, $line->back_date)
                    ->setCellValue('U' . $startLine, $line->commit)
                    ->setCellValue('V' . $startLine, $line->force_commit)
                    ->setCellValue('W' . $startLine, date('Y-m-d H:i:s', $line->createtime));
                $startLine++;
            }

        }
       // dump($sheet);
        $writer = new Xlsx($sheet);
        return response($writer->save('php://output'))->header([
            'Content-Type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'Cache-Control' => 'max-age=0',
            'Content-Disposition' => 'attachment;filename='.$filename,
            //
        ]);

    }
}