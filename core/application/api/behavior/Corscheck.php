<?php
/**
 * Created by PhpStorm.
 * User: HKC
 * Date: 2019/3/13
 * Time: 20:02
 */

namespace app\api\behavior;

use think\Response;

class Corscheck
{
    public function run($dispatch)
    {
        $host_name = isset($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN'] : "*";
        $headers = [
//            "Access-Control-Allow-Origin" => $host_name,
        "Access-Control-Allow-Origin"=>'*',
           // "Access-Control-Allow-Origin" => '*',
            "Access-Control-Allow-Credentials" => 'true',
            "Access-Control-Allow-Headers" => "x-token,x-uid,x-token-check,x-requested-with,content-type,Host",
            "Access-Control-Allow-Methods"=>'POST,GET,OPTIONS,DELETE,PUT'
        ];
        if ($dispatch instanceof Response) {
            $dispatch->header($headers);
        } else if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
            $dispatch['type'] = 'response';
            $response = new Response('', 200, $headers);
            $dispatch['response'] = $response;
        }
    }
}
