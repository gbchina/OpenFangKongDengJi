<?php
/**
 * Created by PhpStorm.
 * User: HKC
 * Date: 2019/3/27
 * Time: 20:31
 */
/**
 * @param $ROW_DATA
 * @return mixed
 * @desc 获取以json提交的POST请求
 */
function json_post()
{
    return json_decode(file_get_contents("php://input"));
}

/**
 * UTC时间转为UNIX时间戳
 * @param $utc
 * @return false|int
 */
function utc2unixdatetime($utc)
{
    date_default_timezone_set('PRC');
    return strtotime($utc);

    //   $time = str_replace(array('T', 'Z'), ' ', $utc);
    //  return $unix = strtotime($time);
}

//正负数相互转换
function plus_minus_conversion($number = 0)
{
    return $number > 0 ? -1 * $number : abs($number);
}

/**
 * 求两个已知经纬度之间的距离,单位为米
 *
 * @param lng1 $ ,lng2 经度
 * @param lat1 $ ,lat2 纬度
 * @return float 距离，单位米
 * @author www.Alixixi.com
 */
function getdistance($lng1, $lat1, $lng2, $lat2)
{
    $lng1 = (is_null($lng1) && empty($lng1)) ? -1 : (float)$lng1;
    $lat1 = (is_null($lat1) && empty($lat1)) ? -1 : (float)$lat1;
    $lng2 = (is_null($lng2) && empty($lng2)) ? -1 : (float)$lng2;
    $lat2 = (is_null($lat2) && empty($lat2)) ? -1 : (float)$lat2;
    // 将角度转为狐度

    $radLat1 = deg2rad($lat1); //deg2rad()函数将角度转换为弧度
    $radLat2 = deg2rad($lat2);
    $radLng1 = deg2rad($lng1);
    $radLng2 = deg2rad($lng2);
    $a = $radLat1 - $radLat2;
    $b = $radLng1 - $radLng2;
    $s = 2 * asin(sqrt(pow(sin($a / 2), 2) + cos($radLat1) * cos($radLat2) * pow(sin($b / 2), 2))) * 6378.137;
    return $s;
}
 function uuid()
{
    return sprintf(
        '%04x%04x-%04x-%04x-%04x-%04x%04x%04x', mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0x0fff) | 0x4000, mt_rand(0, 0x3fff) | 0x8000, mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
    );
}
function timediff($begin_time,$end_time)
{
    if($begin_time < $end_time){
        $starttime = $begin_time;
        $endtime = $end_time;
    }else{
        $starttime = $end_time;
        $endtime = $begin_time;
    }

    //计算天数
    $timediff = $endtime-$starttime;
    $days = intval($timediff/86400);
    //计算小时数
    $remain = $timediff%86400;
    $hours = intval($remain/3600);
    //计算分钟数
    $remain = $remain%3600;
    $mins = intval($remain/60);
    //计算秒数
    $secs = $remain%60;
    $res = array("day" => $days,"hour" => $hours,"min" => $mins,"sec" => $secs);
    return $res;
}


